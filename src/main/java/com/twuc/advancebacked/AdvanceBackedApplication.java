package com.twuc.advancebacked;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvanceBackedApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdvanceBackedApplication.class, args);
    }

}
